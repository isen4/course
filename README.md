# TP

## Setup

Install <https://www.docker.com/products/docker-desktop>, to allow you to test docker images and create the Dockerfile.

Install <https://code.visualstudio.com/> + <https://marketplace.visualstudio.com/items?itemName=GitLab.gitlab-workflow> to have a gitlab-ci linter.
Or use the gitlab pipeline editor <https://docs.gitlab.com/ee/ci/pipeline_editor/>.

Sign in to <https://gitlab.com/> and fork <https://gitlab.com/isen4/spring-petclinic/-/forks/new> inside the personnal course namespace.

Create your gitlab runner, go to your group -> Build -> Runners -> New group runner. Check `Run untagged jobs` then Create runner.
Inside the Step 1 you should see a token, copy the file bellow and replace `your token` with the token value.

```toml
concurrent = 30
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "76d7b2cb5464"
  url = "https://gitlab.com/"
  id = 20394017
  token = "your token"
  token_obtained_at = 2023-01-13T08:48:04Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  limit = 0
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
  [runners.docker]
    tls_verify = false
    image = "alpine"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
    pull_policy = "if-not-present"
```

From your wsl, save it in `/srv/gitlab-runner/config/config.toml`.

Then run this command:

```sh
docker run -d --name gitlab-runner --restart always \
  -v /srv/gitlab-runner/config:/etc/gitlab-runner \
  -v /var/run/docker.sock:/var/run/docker.sock \
  gitlab/gitlab-runner:latest
```

You should be able to see your runner inside the gitlab ui.

## Step 1

In this step 1 you will create a **stage build** which contains the **maven job** in which you will **package the application**.

## Step 2

In this step you will add cache to your pipeline to optimize the build, use this variable **MAVEN_OPTS** to change the maven repository directory:

```yaml
MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"
```

Then you will add **target/** to your **artifacts** to share files through the pipeline.

## Step 3

In this step we are going to **create our Dockerfile** use as parent image **gcr.io/distroless/java:11**.

## Step 4

Add a new **stage called package** in which we are going to build our docker image.
Create a **job kaniko**, [kaniko](https://github.com/GoogleContainerTools/kaniko) is a tool that allow you to build docker image without docker daemon, we call it docker daemonless tools. We use this kind of tool for security, having access to a shared docker daemon can be dangerous because there is no restriction of rights (you can see everythings) that is why we use this type of tools.
Use the **predifined gitlab variable** for you image name and tag.

<details close><summary>Script block to unroll if blocked:</summary>

```yaml
  script:
    # create a folder to put docker secrets
    - mkdir -p /kaniko/.docker
    # create the secret of the gitlab registry
    - printf '{"auths":{"%s":{"auth":"%s"}}}' "${CI_REGISTRY}" "$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')" > /kaniko/.docker/config.json
    # run kaniko
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}" # specify the project directory
      --dockerfile "${CI_PROJECT_DIR}/Dockerfile" # specify the Dockerfile path
      --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}" # specify the docker image name
```

</details>

## Step 5

Add a new stage called **analysis**, in this stages we will add 4 jobs:

- sonarqube, is used to static scan the java code.
Create your sonar cloud organization (skip and create manually) then create a project with the key **spring-petclinic-yourlogin**, then go to Branches and change the main branch by main:
![sonar main branch](image/../images/sonar-main-branch.png)

Create the gitlab project variables related to the organization and project key to be able to use them inside the sonarqube job.
Also create a variable which contains the SONAR_URL <https://sonarcloud.io/>.

After that create a personnal token:
![sonar token](image/../images/sonar-token.png)

Create the gitlab project variable related to the token (SONAR_TOKEN).

Use the maven plugin sonar inside your job

<details close><summary>sonarqube script block to unroll if blocked:</summary>

```yaml
  script:
    - ./mvnw $MAVEN_CLI_OPTS sonar:sonar
      -Dsonar.host.url=$SONAR_HOST_URL
      -Dsonar.organization=$SONAR_ORGANIZATION
      -Dsonar.projectKey=$SONAR_PROJECT_KEY
```

</details>

- dependency-check, is used to scan the application dependencies and check CVE. Use the official docker image, **export the reports (dependency-check folder) inside artifacts** and **cache dependency-check/data**.
To create the **job dependency-check** you can use this commands:

```yaml
- mkdir -p dependency-check/data
- /usr/share/dependency-check/bin/dependency-check.sh
  --scan .
  --format 'ALL' # all kind of cve
  --project "$TO_CHANGE" # project name use a predifined gitlab ci variable
   --data=dependency-check/data # folder for cache
   --out=dependency-check # folder of the reports
```

Don't forget to change the variable `$TO_CHANGE`. Also there is an **entrypoint** by default inside the official image don't forget to **overide it**.

<details close><summary>dependency-check image block to unroll if blocked:</summary>
owasp/dependency-check
</details>

- trivy (is a docker damonless tools), is used to scan the docker image and check CVEs. Use the official docker image, the job will fail due to CVEs so allow it to fail and export the report inside artifacts

```yaml
variables:
  GIT_STRATEGY: none # when set to none the project is not cloned
  TRIVY_USERNAME: "$TO_CHANGE" # registry user
  TRIVY_PASSWORD: "$TO_CHANGE" # registry password
  TRIVY_AUTH_URL: "$TO_CHANGE" # registry url
  FULL_IMAGE_NAME: $TO_CHANGE
script:
    # cache cleanup is needed when scanning images with the same tags, it does not remove the database
    - time trivy clean --scan-cache
    # update vulnerabilities db
    - time trivy image --download-db-only --no-progress --cache-dir .trivycache/
    # Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
    - time trivy image --exit-code 0 --cache-dir .trivycache/ --no-progress --format template --template "@/contrib/gitlab.tpl"
      --output "$CI_PROJECT_DIR/gl-container-scanning-report.json" "$FULL_IMAGE_NAME"
    # Prints full report
    - time trivy image --exit-code 0 --cache-dir .trivycache/ --no-progress "$FULL_IMAGE_NAME"
    # Fail on critical vulnerabilities
    - time trivy image --exit-code 1 --cache-dir .trivycache/ --severity CRITICAL --no-progress "$FULL_IMAGE_NAME"
  cache:
    paths:
      - .trivycache/
  artifacts:
    when: always
    reports:
      container_scanning: gl-container-scanning-report.json
```

Don't forget to change the variables `$TO_CHANGE`. Also there is an **entrypoint** by default inside the official image don't forget to **overide it**.

<details close><summary>trivy image block to unroll if blocked:</summary>
aquasec/trivy
</details>

- gitlab-ci comes with pre-build job like **secret_detection**, include it

<details close><summary>secret_detection script block to unroll if blocked:</summary>

```yaml
include:
  - template: Security/Secret-Detection.gitlab-ci.yml

secret_detection:
  stage: analysis
  variables:
    SECRET_DETECTION_HISTORIC_SCAN: "true"
```

</details>

# Step 6

Add a new **stage deploy**, with the job **stagging**.
In this job use the **multi pipeline project** and trigger another pipeline.
Create a new project in which we will deploy our application. Change the default branch to `main`.
And refer it to the trigger section of your job.
Use conditions to run this pipeline **only on main branch**.
Create this token <https://docs.gitlab.com/ee/user/project/deploy_tokens/#gitlab-deploy-token> this will help us to pull the docker image (read registry permission).
If you are using gitlab group you have to create `CI_DEPLOY_USER` and `CI_DEPLOY_PASSWORD` related to the generated values.
If you are not using gitlab group send this variables like bellow.
Send this variables (adds a suffix otherwise they will be overloaded by the variables of the new pipeline) to the child pipeline:

- $CI_REGISTRY_IMAGE
- $CI_COMMIT_REF_SLUG

With this group of variables, we have everything we need to deploy our docker image.

# Step 7

For this step request the docker engine variables to the speaker.

Add this block:

```yaml
before_script:
  - mkdir -p '/root/.docker'
  - cp "$DOCKER_CERT_CA" '/root/.docker/ca.pem'
  - cp "$DOCKER_CERT_CERT" '/root/.docker/cert.pem'
  - cp "$DOCKER_CERT_KEY" '/root/.docker/key.pem'

# this is used as default value, will be overide by the parent pipeline
variables:
  PARENT_CI_REGISTRY_IMAGE: registry.gitlab.com/your_image
  PARENT_CI_COMMIT_REF_SLUG: main
```

Create a stage `deploy`.

Create a job `docker` inside the `deploy` stage:

```yaml
docker:
  ...
  image: docker:20.10.12
  script:
    # login to your gitlab docker registry
    - docker login -u "$CI_DEPLOY_USER" -p "$CI_DEPLOY_PASSWORD" "$CI_REGISTRY"
    # remove previous deployment
    - docker rm -f $(docker ps --filter "label=GITLAB_USER_LOGIN=$GITLAB_USER_LOGIN" -aq) || true
    # pull the latest version
    - docker pull "$PARENT_CI_REGISTRY_IMAGE:$PARENT_CI_COMMIT_REF_SLUG"
    # run the container
    - docker run -l "GITLAB_USER_LOGIN=$GITLAB_USER_LOGIN" -l "CI_PIPELINE_ID=$CI_PIPELINE_ID" -P -d "$PARENT_CI_REGISTRY_IMAGE:$PARENT_CI_COMMIT_REF_SLUG"
    # get container status
    - docker ps --filter "label=CI_PIPELINE_ID=$CI_PIPELINE_ID" -a
    #get container port
    - docker port $(docker ps --filter "label=CI_PIPELINE_ID=$CI_PIPELINE_ID" -aq) 8080
    - ...
  ...
```

Set the port of you application as a report dotenv.
Command to get the port:

```bash
docker port $(docker ps --filter "label=CI_PIPELINE_ID=$CI_PIPELINE_ID" -aq) 8080 | head -1 |cut -d':' -f2
```

Set the enviroment block with the url `http://$DOCKER_IP:$DOCKER_PORT` and with on_stop job.

Create the job `stop_docker`:

```yaml
stop_docker:
  stage: .post
  script:
    # remove docker container
    - docker rm -f $(docker ps --filter "label=GITLAB_USER_LOGIN=$GITLAB_USER_LOGIN" -aq) || true
```

Run this job when manual and add the environement section with a stop action.

When everything is working, add a workflow step to run the pipeline only on pipeline source `pipeline`.

# Step 8

Add a dotenv artifact on the **docker** job to export the **docker port** value inside the variable `DOCKER_PORT`.

Create a new job after the **docker** job than echo this variable.

# Step 9

**Export your pipeline** inside a **dedicated gitlab project** called **maven-pipeline** to make it reusable.
And **include** it inside **spring-petclinic**.

# Step 10

**Update the kaniko and trivy** jobs to **run** only if a **Dockerfile exist**.

# Step 11

**dependency-check** and **sonarqube** are slow, I want a pipeline under 10 minutes.
Improve it using **needs** and set **dependencies: []** on the jobs that doesnt need artifacts.

# Step 12

Create a release job like <https://docs.gitlab.com/ee/user/project/releases/release_cicd_examples.html#create-a-release-when-a-commit-is-merged-to-the-default-branch> in a new stage, this job must be **manual**.

Disable tag pipeline using **workflow**:

```yaml
workflow:
  rules:
    - if: ...
      when: never
```